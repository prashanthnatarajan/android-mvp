package com.saltmines.mvp.ui.main;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.saltmines.mvp.R;
import com.saltmines.mvp.ui.login.LoginMvpView;
import com.saltmines.mvp.ui.login.LoginPresenter;

public class MainActivity extends AppCompatActivity {

    public static final String KEY_USERNAME = "username";
    private String mUserName;
    private TextView mTxtUsername;

    /**
     * Method which returns the current activity Intent object.
     * @param context
     * @param username
     * @return
     */
    public static Intent getIntent(Context context , String username){
        Intent intent = new Intent(context , MainActivity.class);
        intent.putExtra(KEY_USERNAME , username);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle b = getIntent().getExtras();
        if(b != null){
            mUserName = b.getString(KEY_USERNAME);
        }
        setContentView(R.layout.activity_main);
        mTxtUsername = (TextView) findViewById(R.id.txt_username);
        mTxtUsername.setText("Welcome "+mUserName);
    }
}
