package com.saltmines.mvp.ui.login;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.saltmines.mvp.R;
import com.saltmines.mvp.ui.base.BaseActivity;
import com.saltmines.mvp.ui.main.MainActivity;

/**
 * Created by Prashanth on 22-02-2017.
 */

public class LoginActivity extends BaseActivity implements LoginMvpView , View.OnClickListener{

    private LoginPresenter<LoginMvpView> mPresenter;
    private EditText mEdtUsername;
    private EditText mEdtPwd;
    private Button mBtnLogin;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mEdtUsername = (EditText) findViewById(R.id.et_username);
        mEdtPwd = (EditText) findViewById(R.id.et_password);
        mBtnLogin = (Button) findViewById(R.id.btn_server_login);
        mPresenter = new LoginPresenter<>();
        mPresenter.attachView(this);
        mBtnLogin.setOnClickListener(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
    }

    @Override
    public void openMainActivity() {
        startActivity(MainActivity.getIntent(this , mEdtUsername.getText().toString()));
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_server_login:
                String username = mEdtUsername.getText().toString();
                String password = mEdtPwd.getText().toString();
                if(mPresenter.onValidation(username , password)){
                    mPresenter.onLoginClick(username , password);
                }

                break;

            default:break;
        }
    }
}
