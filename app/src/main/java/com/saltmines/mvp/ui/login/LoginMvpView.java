package com.saltmines.mvp.ui.login;

import com.saltmines.mvp.ui.base.MvpView;

/**
 * Created by Prashanth on 22-02-2017.
 */

public interface LoginMvpView extends MvpView{
    void openMainActivity();
}
