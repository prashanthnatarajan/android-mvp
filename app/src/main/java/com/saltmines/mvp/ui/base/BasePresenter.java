package com.saltmines.mvp.ui.base;

import com.saltmines.mvp.MvpApplication;
import com.saltmines.mvp.data.AppDataManager;
import com.saltmines.mvp.data.DataManager;

/**
 * Created by Prashanth on 22-02-2017.
 */

public class BasePresenter<V extends MvpView> implements MvpPresenter<V> {

    private V mBaseView;
    private final DataManager mDataManager;

    public BasePresenter() {
        mDataManager = AppDataManager.getInstance(MvpApplication.getInstance());
    }

    @Override
    public void attachView(V baseView) {
        mBaseView = baseView;
    }

    @Override
    public void detachView() {
        mBaseView = null;
    }

    @Override
    public boolean isViewAttached() {
        return mBaseView != null;
    }

    /**
     * Method which returns the attached view to the presenter.
     * @return
     */
    public V getMvpView() {
        return mBaseView;
    }

    /**
     * Method to check whether the view is attached. This method throws runtime exception.
     */
    public void checkViewAttached() {
        if (!isViewAttached()) throw new MvpViewNotAttachedException();
    }

    /**
     * Method which returns the DataManager instance.
     * @return
     */
    public DataManager getDataManager() {
        return mDataManager;
    }

    /**
     * Static inner class to create custom runtime exception.
     */
    public static class MvpViewNotAttachedException extends RuntimeException {
        public MvpViewNotAttachedException() {
            super("Please call Presenter.attachView(MvpView) before" +
                    " requesting data to the Presenter");
        }
    }
}
