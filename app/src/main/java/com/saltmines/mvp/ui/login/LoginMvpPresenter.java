package com.saltmines.mvp.ui.login;

/**
 * Created by Prashanth on 23-02-2017.
 */

public interface LoginMvpPresenter {
    void onLoginClick(String username , String password);
    boolean onValidation(String username , String password);
}
