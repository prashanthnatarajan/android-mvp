package com.saltmines.mvp.ui.base;

import android.content.Context;
import android.support.v4.app.Fragment;

import com.saltmines.mvp.utils.NetworkUtils;

/**
 * Created by Prashanth on 22-02-2017.
 */

public abstract class BaseFragment extends Fragment implements MvpView{

    private BaseActivity mActivity;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof BaseActivity) {
            BaseActivity activity = (BaseActivity) context;
            this.mActivity = activity;
        }
    }

    @Override
    public void onDetach() {
        mActivity = null;
        super.onDetach();
    }


    @Override
    public void showProgressDialog() {
        if (mActivity != null) {
            mActivity.showProgressDialog();
        }
    }

    @Override
    public void cancelProgressDialog() {
        if (mActivity != null) {
            mActivity.cancelProgressDialog();
        }
    }

    @Override
    public void showErrorDialog(String message) {
        if (mActivity != null) {
            mActivity.showErrorDialog(message);
        }
    }

    @Override
    public boolean isNetworkConnected() {
        if (mActivity != null) {
            return mActivity.isNetworkConnected();
        }
        return false;
    }

    @Override
    public void hideKeyboard() {
        if (mActivity != null) {
            mActivity.hideKeyboard();
        }
    }

}
