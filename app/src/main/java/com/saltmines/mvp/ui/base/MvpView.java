package com.saltmines.mvp.ui.base;

/**
 * Created by Prashanth on 22-02-2017.
 */

public interface MvpView {

    void showProgressDialog();

    void cancelProgressDialog();

    void showErrorDialog(String message);

    boolean isNetworkConnected();

    void hideKeyboard();
}
