package com.saltmines.mvp.ui.login;

import com.saltmines.mvp.data.network.model.User;
import com.saltmines.mvp.ui.base.BasePresenter;

/**
 * Created by Prashanth on 22-02-2017.
 */

public class LoginPresenter<V extends LoginMvpView> extends BasePresenter<LoginMvpView>
        implements LoginMvpPresenter{

    public LoginPresenter(){
        super();
    }

    @Override
    public void onLoginClick(String username, String password) {
        // Call Login Api and get response
        User user = getDataManager().loginApi();
        // Instruct LoginActivity to navigate to MainActivity.
        getMvpView().openMainActivity();
    }

    @Override
    public boolean onValidation(String username, String password) {
        if(username.length() == 0){
           getMvpView().showErrorDialog("Please enter your username");
            return false;
        }
        if(password.length() == 0){
            getMvpView().showErrorDialog("Please enter your password");
            return false;
        }
        return true;
    }
}
