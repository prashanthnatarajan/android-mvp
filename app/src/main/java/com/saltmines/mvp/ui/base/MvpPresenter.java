package com.saltmines.mvp.ui.base;

/**
 * Created by Prashanth on 22-02-2017.
 */

public interface MvpPresenter<V extends MvpView> {

    /**
     * Method to set the view reference for implemented presenter.
     * @param mvpView
     */
    void attachView(V mvpView);

    /**
     * Method to detach the view from the presenter.
     */
    void detachView();

    /**
     * Method to identify whether the view is addacted to the presenter or not.
     * @return
     */
    boolean isViewAttached();

}
