package com.saltmines.mvp;

import android.app.Application;
import android.content.Context;

import com.saltmines.mvp.data.AppDataManager;

/**
 * Created by Prashanth on 22-02-2017.
 */

public class MvpApplication extends Application{

    private static MvpApplication mMvpApplication;

    public static MvpApplication getInstance(){
        return mMvpApplication;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mMvpApplication = this;
        AppDataManager.getInstance(getAppContext());
    }
    /**
     * Method which returns the application context.
     * @return
     */
    public  Context getAppContext(){
        return getApplicationContext();
    }
}
