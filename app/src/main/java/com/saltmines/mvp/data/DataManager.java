package com.saltmines.mvp.data;

import com.saltmines.mvp.data.db.DbHelper;
import com.saltmines.mvp.data.network.ApiHelper;
import com.saltmines.mvp.data.prefs.PreferenceHelper;

/**
 * Created by Prashanth on 22-02-2017.
 */

public interface DataManager extends DbHelper , ApiHelper , PreferenceHelper{
}
