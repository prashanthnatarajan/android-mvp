package com.saltmines.mvp.data.db;

import android.content.Context;

import com.saltmines.mvp.data.network.model.Configurations;
import com.saltmines.mvp.data.network.model.User;

/**
 * Created by Prashanth on 22-02-2017.
 */

public class AppDbHelper implements DbHelper{

    private static AppDbHelper mAppDbHelper;
    private Database mDatabase;

    public static AppDbHelper getInstance(Context context){
        if(mAppDbHelper == null){
            mAppDbHelper = new AppDbHelper(context);
        }
        return mAppDbHelper;
    }

    private AppDbHelper(Context context){
        mDatabase = Database.getDbInstance();
    }


    @Override
    public void insertConfigs(Configurations configurations) {

    }

    @Override
    public void insertUserDetails(User user) {

    }
}
