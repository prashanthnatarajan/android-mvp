package com.saltmines.mvp.data.network;

import com.saltmines.mvp.data.network.model.Configurations;
import com.saltmines.mvp.data.network.model.User;

/**
 * Created by Prashanth on 22-02-2017.
 */

public class AppApiHelper implements ApiHelper{

    private static AppApiHelper mAppApiHelper;

    private AppApiHelper(){

    }

    public static AppApiHelper getInstance(){
        if(mAppApiHelper == null){
            mAppApiHelper = new AppApiHelper();
        }
        return mAppApiHelper;
    }

    @Override
    public Configurations getConfigs() {
        return null;
    }

    @Override
    public User loginApi() {
        return null;
    }
}
