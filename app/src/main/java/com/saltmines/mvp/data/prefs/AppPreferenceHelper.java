package com.saltmines.mvp.data.prefs;

import android.content.Context;

/**
 * Created by Prashanth on 22-02-2017.
 */

public class AppPreferenceHelper implements PreferenceHelper{

    private static AppPreferenceHelper mAppPreferenceHelper;

    private AppPreferenceHelper(Context context){

    }

    public static AppPreferenceHelper getInstance(Context context){
        if(mAppPreferenceHelper == null){
            mAppPreferenceHelper = new AppPreferenceHelper(context);
        }
        return mAppPreferenceHelper;
    }

    @Override
    public void saveUserId(String userid) {

    }

    @Override
    public String getUserId() {
        return null;
    }
}
