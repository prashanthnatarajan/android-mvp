package com.saltmines.mvp.data.prefs;

/**
 * Created by Prashanth on 22-02-2017.
 */

public interface PreferenceHelper {

    void saveUserId(String userid);
    String getUserId();

}
