package com.saltmines.mvp.data.network;

import com.saltmines.mvp.data.network.model.Configurations;
import com.saltmines.mvp.data.network.model.User;

/**
 * Created by Prashanth on 22-02-2017.
 */

public interface ApiHelper {
    Configurations getConfigs();
    User loginApi();
}
