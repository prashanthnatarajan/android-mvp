package com.saltmines.mvp.data;

import android.content.Context;

import com.saltmines.mvp.data.db.AppDbHelper;
import com.saltmines.mvp.data.db.DbHelper;
import com.saltmines.mvp.data.network.ApiHelper;
import com.saltmines.mvp.data.network.AppApiHelper;
import com.saltmines.mvp.data.network.model.Configurations;
import com.saltmines.mvp.data.network.model.User;
import com.saltmines.mvp.data.prefs.AppPreferenceHelper;
import com.saltmines.mvp.data.prefs.PreferenceHelper;

/**
 * Created by Prashanth on 22-02-2017.
 */

public class AppDataManager implements DataManager{

    private static AppDataManager mAppManager;
    private DbHelper mDbHelper;
    private ApiHelper mApiHelper;
    private PreferenceHelper mPreferenceHelper;


    public static AppDataManager getInstance(Context context){
        if(mAppManager == null){
            mAppManager = new AppDataManager(context);
        }
        return mAppManager;
    }

    private AppDataManager(Context context){
        mDbHelper = AppDbHelper.getInstance(context);
        mApiHelper = AppApiHelper.getInstance();
        mPreferenceHelper = AppPreferenceHelper.getInstance(context);
    }

    @Override
    public void insertConfigs(Configurations configurations) {
        mDbHelper.insertConfigs(configurations);
    }

    @Override
    public void insertUserDetails(User user) {
        mDbHelper.insertUserDetails(user);
    }

    @Override
    public Configurations getConfigs() {
        return null;
    }

    @Override
    public User loginApi() {
        return mApiHelper.loginApi();
    }

    @Override
    public void saveUserId(String userid) {
        mPreferenceHelper.saveUserId(userid);
    }

    @Override
    public String getUserId() {
        return mPreferenceHelper.getUserId();
    }
}
