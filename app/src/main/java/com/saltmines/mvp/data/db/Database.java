package com.saltmines.mvp.data.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.saltmines.mvp.MvpApplication;

/**
 * Created by Prashanth on 22-02-2017.
 */

public class Database extends SQLiteOpenHelper{

    private static final int DB_VERION = 1;
    private static final String DB_NAME = "mvp.db";
    private static Database mDatabase;

    public static Database getDbInstance(){
        if(mDatabase == null){
            mDatabase = new Database(MvpApplication.getInstance());
        }
        return mDatabase;
    }

    private Database(Context context) {
        super(context, DB_NAME, null, DB_VERION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // Create required tables for the application.
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
